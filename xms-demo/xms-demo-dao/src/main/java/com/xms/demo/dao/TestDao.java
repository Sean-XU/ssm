package com.xms.demo.dao;

import com.xms.demo.domain.TestDO;

import java.util.List;

/**
 * Created by xumengsen.
 * Create Time by 2016-10-10 15:55.
 * QQ by 891861712.
 */
public interface TestDao {

    /**
     * 查询测试数据
     *
     * @param testDO
     * @return
     */
    TestDO getTest(TestDO testDO);

    /**
     * 查询测试数据列表
     *
     * @param testDO
     * @return
     */
    List<TestDO> listTest(TestDO testDO);

    /**
     * 查询测试数据总数
     *
     * @param testDO
     * @return
     */
    Object countTest(TestDO testDO);

    /**
     * 新增测试数据
     *
     * @param testDO
     * @return
     */
    boolean saveTest(TestDO testDO);

    /**
     * 更新测试数据
     *
     * @param testDO
     * @return
     */
    boolean updateTest(TestDO testDO);

    /**
     * 删除测试数据
     *
     * @param testDO
     * @return
     */
    boolean removeTest(TestDO testDO);
}
