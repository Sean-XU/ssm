package com.xms.demo.dao.impl;

import com.xms.demo.dao.BaseDao;
import com.xms.demo.dao.TestDao;
import com.xms.demo.domain.TestDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by xumengsen.
 * Create Time by 2016-10-10 15:55.
 * QQ by 891861712.
 */
@Repository
public class TestDaoImpl extends BaseDao implements TestDao {

    private final String namespace = TestDaoImpl.class.getName();

    /**
     * 查询测试数据
     *
     * @param testDO
     * @return
     */
    public TestDO getTest(TestDO testDO) {
        return (TestDO) queryForObject(namespace + ".queryTest", testDO);
    }

    /**
     * 查询测试数据列表
     *
     * @param testDO
     * @return
     */
    public List<TestDO> listTest(TestDO testDO) {
        return queryForList(namespace + ".queryTest", testDO);
    }

    /**
     * 查询测试数据总数
     *
     * @param testDO
     * @return
     */
    public Object countTest(TestDO testDO) {
        return queryCountForObject(namespace + ".countTest", testDO);
    }

    /**
     * 新增测试数据
     *
     * @param testDO
     * @return
     */
    public boolean saveTest(TestDO testDO) {
        return save(namespace + ".saveTest", testDO);
    }

    /**
     * 更新测试数据
     *
     * @param testDO
     * @return
     */
    public boolean updateTest(TestDO testDO) {
        return save(namespace + ".updateTest", testDO);
    }

    /**
     * 删除测试数据
     *
     * @param testDO
     * @return
     */
    public boolean removeTest(TestDO testDO) {
        return save(namespace + ".removeTest", testDO);
    }
}
