package com.xms.demo.common.util;

import javax.servlet.http.HttpServletRequest;
import java.net.*;
import java.util.Enumeration;

/**
 * 获取客户端真实IP
 * Created by xumengsen.
 * Create Time by 2016-10-31 21:09.
 * QQ by 891861712.
 */
public class IpAddress {


    public static String OWN_SIGN_BASE = "BASE";
    /**
     * 本机IP地址的缓存
     */
    private static InetAddress localInetAddressCached = null;


    public static String getIpAddress(HttpServletRequest request) {

        String ip = request.getHeader("x-forwarded-for");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = request.getHeader("Proxy-Client-IP");

        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = request.getHeader("WL-Proxy-Client-IP");

        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = request.getRemoteAddr();

        }

        return ip;

    }


    // 获取本机ip地址
    public static String getIpAddressByLocal() {
        String ip = null;
        try {
            Enumeration allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress inetAddress = null;
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                System.out.println(netInterface.getName());
                Enumeration addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    inetAddress = (InetAddress) addresses.nextElement();
                    if (inetAddress != null && inetAddress instanceof Inet4Address) {
                        ip = inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {

        }
        return ip;
    }

    /**
     * 获取本机主机名
     *
     * @return 本机主机名
     */
    public static String getLocalHostName() {
        try {
            if (null == localInetAddressCached) {
                localInetAddressCached = getLocalInetAddress();
            }
            return localInetAddressCached.getHostName();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取本机主机IP地址
     *
     * @return 本机主机IP地址
     */
    public static String getLocalIP() {
        try {
            if (null == localInetAddressCached) {
                localInetAddressCached = getLocalInetAddress();
            }
            return localInetAddressCached.getHostAddress();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取本机IP地址信息
     *
     * @return
     * @throws UnknownHostException
     * @throws SocketException
     */
    public static InetAddress getLocalInetAddress() throws UnknownHostException, SocketException {
        if (isWindowsOS()) {
            return InetAddress.getLocalHost();
        }

        InetAddress netAddress = null;// 外网地址
        InetAddress localAddress = null;// 本地地址，如果没有配置外网地址则返回它
        Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
        boolean finded = false;// 是否找到外网地址
        while (netInterfaces.hasMoreElements() && !finded) {
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> addresses = ni.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress address = addresses.nextElement();
                if (!address.isSiteLocalAddress() && !address.isLoopbackAddress()
                        && address.getHostAddress().indexOf(":") == -1) {// 外网IP
                    netAddress = address;
                    finded = true;
                    break;
                } else if (address.isSiteLocalAddress() && !address.isLoopbackAddress()
                        && address.getHostAddress().indexOf(":") == -1) {// 内网IP
                    localAddress = address;
                }
            }
        }

        if (netAddress != null && finded) {
            return netAddress;
        } else {
            return localAddress;
        }
    }

    /**
     * 判断是否是window操作系统
     *
     * @return
     */
    public static boolean isWindowsOS() {
        boolean isWindowsOS = false;
        String osName = System.getProperty("os.name");
        if (osName.toLowerCase().indexOf("windows") > -1) {
            isWindowsOS = true;
        }
        return isWindowsOS;
    }

}
