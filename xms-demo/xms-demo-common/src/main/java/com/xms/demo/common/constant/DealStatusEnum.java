package com.xms.demo.common.constant;

import lombok.Getter;

/**
 * Created by xumengsen.
 * Create Time by 2017-04-01 17:36.
 * QQ by 891861712.
 */
public enum DealStatusEnum {

    SUCCESS(1, "success"), FAILURE(-1, "failure"), JSON_ERROR(10000, "json格式错误"), JSON_ERROR_EMPTY(10001, "json为空"), JSON_ERROR_SYSTEM_NAME(10002, "json签名接口参数错误"), JSON_ERROR_TIME(10003, "json时间验证错误"), JSON_ERROR_SIGN(10004, "json签名验证错误"),
    DATA_ERROR_EXISTED(20000, "数据已存在");

    DealStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Getter
    private final int code;

    @Getter
    private final String message;
}
