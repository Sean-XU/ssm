package com.xms.demo.common.domain;

import lombok.Data;

/**
 * api返回结果实体
 * Created by xumengsen.
 * Create Time by 2016-10-19 10:17.
 * QQ by 891861712.
 */
@Data
public class ReturnResultDTO {

    //返回状态码
    private Integer code;

    //是否重试（0：否；1：是）//默认值0
    private Integer retry;

    //返回错误
    private String message;

    //返回数据
    private Object data;

    //错误原因
    private String error;
}
