package com.xms.demo.common.constant;

/**
 * Created by xumengsen.
 * Create Time by 2017-03-08 18:11.
 * QQ by 891861712.
 */
public class ConfigConsts {

    //设置超时时间
    public static final int TIME_OUT = 10000;

    //字符集
    public static final String CHARSET = "UTF-8";
}
