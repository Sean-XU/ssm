package com.xms.demo.common.util;

import com.alibaba.fastjson.JSON;
import com.xms.demo.common.domain.ReturnResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 返回消息
 * Created by xumengsen.
 * Create Time by 2017-03-20 11:12.
 * QQ by 891861712.
 */
public class MessageUtils {

    //记录日志
    private static final Logger logger = LoggerFactory.getLogger(MessageUtils.class);

    /**
     * 返回成功消息
     *
     * @param sb         日志拼接
     * @param code       成功状态码
     * @param msg        成功消息
     * @param businessNo 业务单号
     */
    public String success(StringBuilder sb, Integer code, String msg, Object data, String businessNo) {
        ReturnResultDTO returnResultDTO = new ReturnResultDTO();
        returnResultDTO.setCode(code);
        returnResultDTO.setMessage(msg);
        returnResultDTO.setData(data);
        sb.append("业务单号：" + businessNo);
        sb.append("操作成功");
        logger.info(sb.toString());
        return JSON.toJSONString(returnResultDTO);
    }

    /**
     * 返回失败消息
     *
     * @param sb         日志拼接
     * @param code       失败状态码
     * @param error      错误原因
     * @param businessNo 业务单号
     * @param body       报文
     */
    public String failure(StringBuilder sb, Integer code, String error, String businessNo, String body) {
        ReturnResultDTO returnResultDTO = new ReturnResultDTO();
        returnResultDTO.setCode(code);
        returnResultDTO.setError(error);
        sb.append("业务单号：" + businessNo);
        sb.append("错误报文：" + body);
        sb.append("错误原因：" + error);
        logger.error(sb.toString());
        return JSON.toJSONString(returnResultDTO);
    }
}
