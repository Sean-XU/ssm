package com.xms.demo.common.util;

import com.alibaba.fastjson.JSON;
import com.xms.demo.common.constant.ConfigConsts;
import com.xms.demo.common.domain.ReturnResultDTO;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by xumengsen.
 * Create Time by 2016-11-07 11:00.
 * QQ by 891861712.
 */
public class HttpRequestUtils {

    /**
     * 发送 get请求
     */
    public static String doGet(String url) throws Exception {
        String result = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            //创建httpGet
            HttpGet httpGet = new HttpGet(url);
            //设置超时时间
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(ConfigConsts.TIME_OUT).setConnectionRequestTimeout(ConfigConsts.TIME_OUT)
                    .setSocketTimeout(ConfigConsts.TIME_OUT).build();
            httpGet.setConfig(requestConfig);
            //执行get请求
            CloseableHttpResponse response = httpClient.execute(httpGet);
            int code = response.getStatusLine().getStatusCode();
            try {
                if (code == 200) {
                    HttpEntity entity = response.getEntity();
                    result = EntityUtils.toString(entity, ConfigConsts.CHARSET);
                } else {
                    //将错误信息转化json格式
                    ReturnResultDTO returnResultDTO = new ReturnResultDTO();
                    returnResultDTO.setCode(code);
                    returnResultDTO.setError("响应失败！响应码：" + code + "");
                    result = JSON.toJSONString(returnResultDTO);
                }
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            throw e;
        } catch (ParseException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            // 关闭连接,释放资源
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }

        return result;
    }

    /**
     * 发送 post请求访问本地应用并根据传递参数不同返回不同结果
     */
    public static String doPost(String url, String body) throws Exception {
        String result = null;
        // 创建默认的httpClient实例.
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建httppost
        HttpPost httpPost = new HttpPost(url);
        try {
            HttpEntity httpEntity = new StringEntity(body, ConfigConsts.CHARSET);
            httpPost.setEntity(httpEntity);
            //设置超时时间
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(ConfigConsts.TIME_OUT).setConnectionRequestTimeout(ConfigConsts.TIME_OUT)
                    .setSocketTimeout(ConfigConsts.TIME_OUT).build();
            httpPost.setConfig(requestConfig);
            //执行post请求
            CloseableHttpResponse response = httpclient.execute(httpPost);
            int code = response.getStatusLine().getStatusCode();
            try {
                if (code == 200) {
                    HttpEntity entity = response.getEntity();
                    result = EntityUtils.toString(entity, ConfigConsts.CHARSET);
                } else {
                    //将错误信息转化json格式
                    ReturnResultDTO returnResultDTO = new ReturnResultDTO();
                    returnResultDTO.setCode(code);
                    returnResultDTO.setError("响应失败！响应码：" + code + "");
                    result = JSON.toJSONString(returnResultDTO);
                }

            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            throw e;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }

        return result;
    }
}
