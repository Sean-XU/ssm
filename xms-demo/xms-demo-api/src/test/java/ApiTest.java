import com.xms.demo.common.util.HttpRequestUtils;
import org.junit.Test;

/**
 * Created by xumengsen.
 * Create Time by 2017-03-15 16:41.
 * QQ by 891861712.
 */
public class ApiTest {

    @Test
    public void getTest() {
        String url = "http://localhost:8080/api/getTest";
        try {
            String result = HttpRequestUtils.doGet(url);
            System.out.print(result);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    @Test
    public void countTest() {
        String url = "http://localhost:8080/api/countTest";
        try {
            String result = HttpRequestUtils.doGet(url);
            System.out.print(result);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    @Test
    public void saveTest() {
        String url = "http://localhost:8080/api/saveTest";
        String body = "{\"remark\":\"test1564654646546545645614645645646\"}";
        try {
            String result = HttpRequestUtils.doPost(url, body);
            System.out.print(result);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    @Test
    public void updateTest() {
        String url = "http://localhost:8080/api/updateTest";
        String body = "{\"id\":\"1\",\"remark\":\"test\"}";
        try {
            String result = HttpRequestUtils.doPost(url, body);
            System.out.print(result);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    @Test
    public void removeTest() {
        String url = "http://localhost:8080/api/removeTest";
        String body = "{\"id\":\"1\"}";
        try {
            String result = HttpRequestUtils.doPost(url, body);
            System.out.print(result);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
