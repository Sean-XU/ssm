package com.xms.demo.api;

import com.xms.demo.common.constant.DealStatusEnum;
import com.xms.demo.common.util.IpAddress;
import com.xms.demo.common.util.MessageUtils;
import com.xms.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;

/**
 * 外部系统调用统一接口（RESTful API，供第三方系统调用）
 * Created by Administrator on 2016-10-26.
 */
@Component
@Path("/api")
@Produces("application/json;charset=UTF-8")
public class Api {

    private MessageUtils messageUtils = new MessageUtils();

    @Autowired
    private TestService testService;

    /**
     * 查询测试数据
     *
     * @param request 获取请求信息
     * @return
     */
    @GET
    @Path("/getTest")
    public String getTest(@Context HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("查询测试数据接口：");
        sb.append("请求端IP:" + IpAddress.getIpAddress(request));
        return testService.getTest(sb);
    }

    /**
     * 查询测试数据
     *
     * @param request 获取请求信息
     * @return
     */
    @GET
    @Path("/countTest")
    public String countTest(@Context HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("查询测试数据总数接口：");
        sb.append("请求端IP:" + IpAddress.getIpAddress(request));
        return testService.countTest(sb);
    }

    /**
     * 新增测试数据
     *
     * @param request 获取请求信息
     * @param body    请求报文
     * @return
     */
    @POST
    @Path("/saveTest")
    public String saveTest(@Context HttpServletRequest request, String body) {
        StringBuilder sb = new StringBuilder();
        sb.append("新增测试数据接口：");
        sb.append("请求端IP:" + IpAddress.getIpAddress(request));
        try {
            return testService.saveTest(sb, body);
        } catch (Exception e) {
            return messageUtils.failure(sb, DealStatusEnum.FAILURE.getCode(), e.toString(), null, body);
        }
    }

    /**
     * 更新测试数据
     *
     * @param request 获取请求信息
     * @param body    请求报文
     * @return
     */
    @POST
    @Path("/updateTest")
    public String updateTest(@Context HttpServletRequest request, String body) {
        StringBuilder sb = new StringBuilder();
        sb.append("更新测试数据接口：");
        sb.append("请求端IP:" + IpAddress.getIpAddress(request));
        try {
            return testService.updateTest(sb, body);
        } catch (Exception e) {
            return messageUtils.failure(sb, DealStatusEnum.FAILURE.getCode(), e.toString(), null, body);
        }
    }

    /**
     * 删除测试数据
     *
     * @param request 获取请求信息
     * @param body    请求报文
     * @return
     */
    @POST
    @Path("/removeTest")
    public String removeTest(@Context HttpServletRequest request, String body) {
        StringBuilder sb = new StringBuilder();
        sb.append("删除测试数据接口：");
        sb.append("请求端IP:" + IpAddress.getIpAddress(request));
        try {
            return testService.removeTest(sb, body);
        } catch (Exception e) {
            return messageUtils.failure(sb, DealStatusEnum.FAILURE.getCode(), e.toString(), null, body);
        }
    }
}

