/*
 Navicat Premium Data Transfer

 Source Server         : dtc-dev-172.16.1.223
 Source Server Type    : MySQL
 Source Server Version : 50626
 Source Host           : 172.16.1.223:3306
 Source Schema         : db_test

 Target Server Type    : MySQL
 Target Server Version : 50626
 File Encoding         : 65001

 Date: 26/10/2018 10:16:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_delete` tinyint(1) NULL DEFAULT 0 COMMENT '是否逻辑删除（0、否；1、是）',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '逻辑删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES (1, '1', '2018-08-30 15:44:04', '2018-08-30 15:44:18', 0, '2018-08-30 15:44:08');

SET FOREIGN_KEY_CHECKS = 1;
