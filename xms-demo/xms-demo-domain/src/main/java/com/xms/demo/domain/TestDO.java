package com.xms.demo.domain;

import lombok.Data;

import java.util.Date;

/**
 * Created by xumengsen.
 * Create Time by 2016-10-10 15:43.
 * QQ by 891861712.
 */
@Data
public class TestDO {

    /**
     * 主键ID
     */
    public Long id;

    /**
     * 备注
     */
    public String remark;

    /**
     * 创建时间
     */
    public Date createTime;

    /**
     * 更新时间
     */
    public Date updateTime;

    /**
     * 是否逻辑删除（0、否；1、是）
     */
    public Integer isDelete;

    /**
     * 逻辑删除时间
     */
    public Date deleteTime;
}
