package com.xms.demo.domain;

import lombok.Data;

import java.util.Date;

/**
 * Created by xumengsen.
 * Create Time by 2016-11-02 16:22.
 * QQ by 891861712.
 */
@Data
public class TaskDO {

    /** 任务ID */
    private Long id;

    /** 环境名称 */
    private String ownSign;

    /** 队列ID */
    private Integer queueId;

    /** 关键字1 */
    private String keyword1;

    /** 关键字2 */
    private String keyword2;

    /** 报文 */
    private String body;

    /** 任务类型 */
    private String taskType;

    /** 任务状态 */
    private Integer taskStatus;

    /** 执行次数 */
    private Integer executeCount;

    /**是否启用（0：否；1：是）*/
    public int isEnabled;

    /**创建人*/
    public String createUser;

    /**创建时间*/
    public Date createTime;

    /**更新人*/
    public String updateUser;

    /**更新时间*/
    public Date updateTime;

    /**更新时间*/
    public Date deleteTime;

    /**备注*/
    public String remark;
}
