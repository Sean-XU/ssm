package com.xms.demo.service;


import javax.servlet.http.HttpServletRequest;

/**
 * Created by xumengsen.
 * Create Time by 2016-10-10 16:15.
 * QQ by 891861712.
 */
public interface TestService {

    /**
     * 查询测试数据
     *
     * @param sb 日志拼接
     * @return
     */
    String getTest(StringBuilder sb);

    /**
     * 查询测试数据总数
     *
     * @param sb 日志拼接
     * @return
     */
    String countTest(StringBuilder sb);

    /**
     * 新增测试数据
     *
     * @param sb   日志拼接
     * @param body 请求报文
     * @return
     * @throws Exception
     */
    String saveTest(StringBuilder sb, String body) throws Exception;

    /**
     * 更新测试数据
     *
     * @param sb   日志拼接
     * @param body 请求报文
     * @return
     * @throws Exception
     */
    String updateTest(StringBuilder sb, String body) throws Exception;

    /**
     * 删除测试数据
     *
     * @param sb   日志拼接
     * @param body 请求报文
     * @return
     * @throws Exception
     */
    String removeTest(StringBuilder sb, String body) throws Exception;

}
