package com.xms.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.xms.demo.common.constant.DealStatusEnum;
import com.xms.demo.common.util.IpAddress;
import com.xms.demo.common.util.MessageUtils;
import com.xms.demo.dao.TestDao;
import com.xms.demo.domain.TestDO;
import com.xms.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by xumengsen.
 * Create Time by 2016-10-10 16:18.
 * QQ by 891861712.
 */
@Service
public class TestServiceImpl implements TestService {

    private MessageUtils messageUtils = new MessageUtils();

    @Autowired
    private TestDao testDao;

    /**
     * 查询测试数据
     *
     * @param sb 日志拼接
     * @return
     */
    public String getTest(StringBuilder sb) {
        try {
            TestDO testDO = new TestDO();
            List<TestDO> testDOList = testDao.listTest(testDO);
            return messageUtils.success(sb, DealStatusEnum.SUCCESS.getCode(), DealStatusEnum.SUCCESS.getMessage(), testDOList, null);
        } catch (Exception e) {
            return messageUtils.failure(sb, DealStatusEnum.FAILURE.getCode(), e.toString(), null, null);
        }
    }

    /**
     * 查询测试数据总数
     *
     * @param sb 日志拼接
     * @return
     */
    public String countTest(StringBuilder sb) {
        try {
            TestDO testDO = new TestDO();
            Object countTest = testDao.countTest(testDO);
            return messageUtils.success(sb, DealStatusEnum.SUCCESS.getCode(), DealStatusEnum.SUCCESS.getMessage(), countTest, null);
        } catch (Exception e) {
            return messageUtils.failure(sb, DealStatusEnum.FAILURE.getCode(), e.toString(), null, null);
        }
    }

    /**
     * 新增测试数据
     *
     * @param sb   日志拼接
     * @param body 请求报文
     * @return
     * @throws Exception
     */
    public String saveTest(StringBuilder sb, String body) throws Exception {
        try {
            TestDO testDO = JSON.parseObject(body, TestDO.class);
            if (testDao.saveTest(testDO)) {
                return messageUtils.success(sb, DealStatusEnum.SUCCESS.getCode(), DealStatusEnum.SUCCESS.getMessage(), null, null);
            }
        } catch (Exception e) {
            throw e;
        }
        return null;
    }

    /**
     * 更新测试数据
     *
     * @param sb   日志拼接
     * @param body 请求报文
     * @return
     * @throws Exception
     */
    public String updateTest(StringBuilder sb, String body) throws Exception {
        try {
            TestDO testDO = JSON.parseObject(body, TestDO.class);
            if (testDao.updateTest(testDO)) {
                return messageUtils.success(sb, DealStatusEnum.SUCCESS.getCode(), DealStatusEnum.SUCCESS.getMessage(), null, null);
            }
        } catch (Exception e) {
            throw e;
        }

        return null;
    }

    /**
     * 删除测试数据
     *
     * @param sb   日志拼接
     * @param body 请求报文
     * @return
     * @throws Exception
     */
    public String removeTest(StringBuilder sb, String body) throws Exception {
        try {
            TestDO testDO = JSON.parseObject(body, TestDO.class);
            if (testDao.removeTest(testDO)) {
                return messageUtils.success(sb, DealStatusEnum.SUCCESS.getCode(), DealStatusEnum.SUCCESS.getMessage(), null, null);
            }
        } catch (Exception e) {
            throw e;
        }

        return null;
    }
}
